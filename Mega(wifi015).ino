/*************************************************** 
  This is an example for the Adafruit CC3000 Wifi Breakout & Shield

  Designed specifically to work with the Adafruit WiFi products:
  ----> https://www.adafruit.com/products/1469

  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!

  Written by Limor Fried & Kevin Townsend for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ****************************************************/
 
 /*
This example does a test of the TCP client capability:
  * Initialization
  * Optional: SSID scan
  * AP connection
  * DHCP printout
  * DNS lookup
  * Optional: Ping
  * Connect to website and print out webpage contents
  * Disconnect
SmartConfig is still beta and kind of works but is not fully vetted!
It might not work on all networks!
*/
#include <Adafruit_CC3000.h>
#include <ccspi.h>
#include <SPI.h>
#include <string.h>
#include "utility/debug.h"

#include <EasyTransfer.h>
#include <dht11.h>
EasyTransfer ET; 
dht11 DHT;
#define DHT11_PIN 40

struct SEND_DATA_STRUCTURE{
 int t;
 int h;
 int s;
 int l;
 int m;
 int d;
 int wi;
 int w;
   int blinks;
  int pause;
};

SEND_DATA_STRUCTURE mydata;

// These are the interrupt and control pins
#define ADAFRUIT_CC3000_IRQ   3  // MUST be an interrupt pin!
// These can be any two pins
#define ADAFRUIT_CC3000_VBAT  5
#define ADAFRUIT_CC3000_CS    10
// Use hardware SPI for the remaining pins
// On an UNO, SCK = 13, MISO = 12, and MOSI = 11
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT,
                                         SPI_CLOCK_DIVIDER); // you can change this clock speed

#define WLAN_SSID       "wifi015"           // cannot be longer than 32 characters!
#define WLAN_PASS       "password"
// Security can be WLAN_SEC_UNSEC, WLAN_SEC_WEP, WLAN_SEC_WPA or WLAN_SEC_WPA2
#define WLAN_SECURITY   WLAN_SEC_WPA2

#define IDLE_TIMEOUT_MS  3000      // Amount of time to wait (in milliseconds) with no data 
                                   // received before closing the connection.  If you know the server
                                   // you're accessing is quick to respond, you can reduce this value.

// What page to grab!
#define WEBSITE      "nature-internet.ivehost.net"
#define WEBPAGE      "/arduino_get.php"


/**************************************************************************/
/*!
    @brief  Sets up the HW and the CC3000 module (called automatically
            on startup)
*/
/**************************************************************************/

uint32_t ip;

int windPin = A0;
int soundPin =  9; //Sound Pin A12
int waterPin = 6;//water Pin A12
int lightPin = 7;//Light Pin A12
int moisturePin = 15;//moisturePin
int voutPin = A8; //Dust Pin
/***Dust setting***/
int ledPin = 46; 
int samplingTime = 280; 
int deltaTime = 40; 
int sleepTime = 9680; 
int voMeasured = 0; 
float calcVoltage = 0; 
float dustDensity = 0;

void setup(void)
{
  
  Serial.begin(9600);
    DHT.read(DHT11_PIN);

   ET.begin(details(mydata), &Serial);

  Serial.println(F("Hello, CC3000!\n")); 

  Serial.print("Free RAM: "); Serial.println(getFreeRam(), DEC);
  
  /* Initialise the module */
  Serial.println(F("\nInitializing..."));
  if (!cc3000.begin())
  {
    Serial.println(F("Couldn't begin()! Check your wiring?"));
    while(1);
  }
  
  // Optional SSID scan
  // listSSIDResults();
  
  Serial.print(F("\nAttempting to connect to ")); Serial.println(WLAN_SSID);
  if (!cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY)) {
    Serial.println(F("Failed!"));
    while(1);
  }
   
  Serial.println(F("Connected!"));
  
  /* Wait for DHCP to complete */
  Serial.println(F("Request DHCP"));
  while (!cc3000.checkDHCP())
  {
    delay(100); // ToDo: Insert a DHCP timeout!
  }  

  /* Display the IP address DNS, Gateway, etc. */  
  while (! displayConnectionDetails()) {
    delay(1000);
  }

  ip = 3397546478;
 
 
  // Optional: Do a ping test on the website
  /*
  Serial.print(F("\n\rPinging ")); cc3000.printIPdotsRev(ip); Serial.print("...");  
  replies = cc3000.ping(ip, 5);
  Serial.print(replies); Serial.println(F(" replies"));
  */  

  /* Try connecting to the website.
     Note: HTTP/1.1 protocol is used to keep the server from closing the connection before all data is read.
  */
//  Adafruit_CC3000_Client www = cc3000.connectTCP(ip, 80);
//  if (www.connected()) {
//    www.fastrprint(F("GET "));
//    www.fastrprint(WEBPAGE);
//    www.fastrprint(F(" HTTP/1.1\r\n"));
//    www.fastrprint(F("Host: ")); www.fastrprint(WEBSITE); www.fastrprint(F("\r\n"));
//    www.fastrprint(F("\r\n"));
//    www.println();
//  } else {
//    Serial.println(F("Connection failed"));    
//    return;
//  }
//
//  Serial.println(F("-------------------------------------"));
//  
//  /* Read data until either the connection is closed, or the idle timeout is reached. */ 
//  unsigned long lastRead = millis();
//  while (www.connected() && (millis() - lastRead < IDLE_TIMEOUT_MS)) {
//    while (www.available()) {
//      char c = www.read();
//      Serial.print(c);
//      lastRead = millis();
//    }
//  }
//  www.close();
//  Serial.println(F("-------------------------------------"));
//  
//  /* You need to make sure to clean up after yourself or the CC3000 can freak out */
//  /* the next time your try to connect ... */
//  Serial.println(F("\n\nDisconnecting"));
//  cc3000.disconnect();
    pinMode(13, OUTPUT);
  pinMode(ledPin,OUTPUT);
}



void loop(void)
{
    
     digitalWrite(ledPin,LOW); // power on the LED
   delayMicroseconds(samplingTime);
   voMeasured = analogRead(voutPin); // read the dust value
   delayMicroseconds(deltaTime);
   digitalWrite(ledPin,HIGH); // turn the LED off
   delayMicroseconds(sleepTime);
  
     int temp = DHT.temperature;
     int hum = DHT.humidity;
     int sound = analogRead(soundPin);
     int light = analogRead(lightPin);
     int moi = analogRead(moisturePin);
     int dust = voMeasured;
     
     int w = analogRead(windPin);
     float outvoltage = w * (5.0 / 1023.0); 
     int win = outvoltage * (50.0 / 3.0);
   
     int wind = win;
     int water =  analogRead(waterPin);
     
  
  mydata.blinks = 3;
  mydata.pause = 3;

     mydata.t = temp;
     mydata.h = hum;
     mydata.s = sound;
     mydata.l = light;
     mydata.m = moi;
     mydata.d = moi;
     mydata.wi = wind;
     mydata.w = water;
     ET.sendData();
        
     for(int i = mydata.blinks; i>0; i--){
      digitalWrite(13, HIGH);
      delay(mydata.pause * 100);
      digitalWrite(13, LOW);
      delay(mydata.pause * 100);
    }
  
     
    Serial.print(temp);
  Adafruit_CC3000_Client www = cc3000.connectTCP(ip, 80);
  if (www.connected()) {
    www.fastrprint(F("GET "));
    www.fastrprint(WEBPAGE);
    
    char buffer[50];
    sprintf(buffer, "?temp=%d", temp);
    www.fastrprint(buffer);
    
    char buffer7[50];
    sprintf(buffer7, "&hum=%d", hum);
    www.fastrprint(buffer7);
    
    char buffer1[50];
    sprintf(buffer1, "&sound=%d", sound);
    www.fastrprint(buffer1);
    
    char buffer2[50];
    sprintf(buffer2, "&light=%d", light);
    www.fastrprint(buffer2);
    
    char buffer3[50];
    sprintf(buffer3, "&moi=%d", moi);
    www.fastrprint(buffer3);
    
    char buffer4[50];
    sprintf(buffer4, "&dust=%d", dust);
    www.fastrprint(buffer4);
    
    char buffer5[50];
    sprintf(buffer5, "&wind=%d", wind);
    www.fastrprint(buffer5);
    
    char buffer6[50];
    sprintf(buffer6, "&water=%d", water);
    www.fastrprint(buffer6);
//    www.fastrprint("?temp=2");
//    www.fastrprint("&&hum=%d", hum);
//    www.fastrprint("&&sound=%d", sound);
//    www.fastrprint("&&light="+light);
//    www.fastrprint("&&moi="+moi);
//    www.fastrprint("&&dust="+dust);
//    www.fastrprint("&&wind="+wind);
//    www.fastrprint("&&water="+water);
    www.fastrprint(F(" HTTP/1.1\r\n"));
    www.fastrprint(F("Host: ")); www.fastrprint(WEBSITE); www.fastrprint(F("\r\n"));
    www.fastrprint(F("\r\n"));
    www.println();
  } else {
    Serial.println(F("Connection failed")); 
    
    return;
  }

  Serial.println(F("-------------------------------------"));
  
  /* Read data until either the connection is closed, or the idle timeout is reached. */ 
  unsigned long lastRead = millis();
  while (www.connected() && (millis() - lastRead < IDLE_TIMEOUT_MS)) {
    while (www.available()) {
      char c = www.read();
      Serial.print(c);
      lastRead = millis();
    }
  }
  www.close();
  Serial.println(F("-------------------------------------"));


 delay(1000);
}

/**************************************************************************/
/*!
    @brief  Begins an SSID scan and prints out all the visible networks
*/
/**************************************************************************/

void listSSIDResults(void)
{
  uint32_t index;
  uint8_t valid, rssi, sec;
  char ssidname[33]; 

  if (!cc3000.startSSIDscan(&index)) {
    Serial.println(F("SSID scan failed!"));
    return;
  }

  Serial.print(F("Networks found: ")); Serial.println(index);
  Serial.println(F("================================================"));

  while (index) {
    index--;

    valid = cc3000.getNextSSID(&rssi, &sec, ssidname);
    
    Serial.print(F("SSID Name    : ")); Serial.print(ssidname);
    Serial.println();
    Serial.print(F("RSSI         : "));
    Serial.println(rssi);
    Serial.print(F("Security Mode: "));
    Serial.println(sec);
    Serial.println();
  }
  Serial.println(F("================================================"));

  cc3000.stopSSIDscan();
}

/**************************************************************************/
/*!
    @brief  Tries to read the IP address and other connection details
*/
/**************************************************************************/
bool displayConnectionDetails(void)
{
  uint32_t ipAddress, netmask, gateway, dhcpserv, dnsserv;
  
  if(!cc3000.getIPAddress(&ipAddress, &netmask, &gateway, &dhcpserv, &dnsserv))
  {
    Serial.println(F("Unable to retrieve the IP Address!\r\n"));
    return false;
  }
  else
  {
    Serial.print(F("\nIP Addr: ")); cc3000.printIPdotsRev(ipAddress);
    Serial.print(F("\nNetmask: ")); cc3000.printIPdotsRev(netmask);
    Serial.print(F("\nGateway: ")); cc3000.printIPdotsRev(gateway);
    Serial.print(F("\nDHCPsrv: ")); cc3000.printIPdotsRev(dhcpserv);
    Serial.print(F("\nDNSserv: ")); cc3000.printIPdotsRev(dnsserv);
    Serial.println();
    return true;
  }
}
