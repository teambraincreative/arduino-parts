#include <SPI.h>
#include <boards.h>
#include <ble_shield.h>
#include <SoftwareSerial.h>
#include <DFPlayer_Mini_Mp3.h>
#include <EasyTransfer.h>
#include <EEPROM.h>

//create object
EasyTransfer ET; 
SoftwareSerial mySerial(3, 4); // RX, TX

struct RECEIVE_DATA_STRUCTURE{
  int t;
  int h;
  int s;
  int l;
  int m;
  int d;
  int wi;
  int w;
  int blinks;
  int pause;
};

RECEIVE_DATA_STRUCTURE mydata;

void setup()
{  
  ble_begin();
  Serial.begin(9600);
  mySerial.begin(9600);
  mp3_set_serial (mySerial);
  mp3_set_volume (30);
  ET.begin(details(mydata), &Serial);  
}

    int temp;
    int hum;
    int sound;
    int light; 
    int moi;
    int dust;
    int wind;
    int water;
    
void loop()
{ 
  if ( ble_available() )
  {
     if(ET.receiveData()){
  
    temp = mydata.t;
    hum = mydata.h;
    sound = mydata.s;
    light = mydata.l; 
    dust = mydata.d;
    wind = mydata.wi;
    water = mydata.w;
}

    while ( ble_available() ){
      int c = ble_read();
      Serial.println(c);

      int ranHello = random(0, 2);
      int ranBye = random(2, 4);
            
      switch(c){

      case 48: //0
        Serial.println("Hello");
        mp3_play(ranHello);
        break;

      case 49: //1
        Serial.write("Temp : ");
        Serial.println(temp);
        mp3_play (temp+100);
        ble_write(c);
        break;

      case 50: //2
        Serial.write("Hum : ");
        Serial.println(hum);
        mp3_play (hum+400);
        ble_write(c);
        break;

      case 51: //3
        Serial.print("Sound : ");
        Serial.println(sound);
        mp3_play (sound+200);
        ble_write(c);
        break;   

      case 52: //4
        Serial.print("Light : ");
        Serial.println(light+300);
        ble_write(c);
        break;

      case 53: //5
        Serial.print("Dust : ");
        if (dust > 300 && dust < 500){
          mp3_play(502);
        } else if (dust > 500 && dust < 800) {
          mp3_play(501);
        }  else if (dust > 800){
          mp3_play(500);
        } else {
          mp3_play(503);
        }
        ble_write(c);
        break;

      case 54: //6 feeling
      Serial.print(wind);
      if (wind > 25 || dust < 300){
      mp3_play(602); //best
      } else if (moi < 300) {
      mp3_play(603); //ok
      } else {
      mp3_play(604); // not good;
      }
              ble_write(c);
        break;
      
      case 55: //7
      Serial.print("intro");
      mp3_play(701);
              ble_write(c);
      break;
   
     case 56: //8
     Serial.print("func");
     mp3_play(702);
             ble_write(c);
  break;   
        
      case 97:
           Serial.print("where");
      mp3_play(704);
              ble_write(c);
        break;
        
              case 98:
           Serial.print("why");
           mp3_play(703);
           ble_write(c);
        break;
        
      case 57: //9
        Serial.println("Bye");
        mp3_play(ranBye);
        break;
        
      }

    }

  }
  ble_do_events();

}

